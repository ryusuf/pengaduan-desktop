var monthtext=['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(), (mm < 10 ? '0':'')+ mm, (dd < 10? '0':'')+ dd].join('-');
};
Vue.filter('formatDate', function (value) {
	var date = value.split("-");
	if (date.length < 3) return value;
	var d = parseInt(date[2]);
	var m = parseInt(date[1]);
	var y = date[0];
	return d+" "+monthtext[m-1]+" "+y;
});
module.exports = new Vue({
    el: '#vm',
    data: {
        isLoading: false,
        title: config.appname + ' version '+ config.version,
        menu: 'dashboard',
        style: {
            background: ''
        },
        prop: {},
        id_pengaduan: [],
    },
    methods: {
        isMenu: function(menu) {
            return menu == this.menu
        },
        changeMenu: function(menu) {
            this.menu == menu
            this.$dispatch('change-menu', menu)
        }
    },
    events: {
        'change-menu': function(menu) {
            this.menu = menu
            this.$broadcast('menu-changed',menu)
        },
        'add-prop': function(prop) {
            this.prop = prop;
        },
    },
    ready: function() {
        this.$dispatch('change-menu', 'dashboard')
    }
});
