Vue.component('menu-frame', {
    template: require('./view.html'),
    data: function () {
        return {
            link: ''
        }
    },
    props: ['prop'],
    computed: {
    },
    methods: {
    },
    events: {
        'frame-changed': function(frame) {
            this.link = config.endpoint + '/' + frame;
        }
    },
    ready: function() {
        this.$dispatch('frame-changed', this.prop.link)
    }
});
