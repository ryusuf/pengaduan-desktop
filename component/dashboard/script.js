Vue.component('dashboard', {
    template: require('./view.html'),
    data: function () {
        return {
            items: ''
        }
    },
    computed: {
    },
    methods: {
        getData: function() {
            var that = this;
            $.getJSON(config.endpoint + '/api/get/data-pengaduan', {
            }, function(data) {
                that.items = data.data;
            });
        },
        frame: function(link) {
            this.$dispatch('change-menu', 'iframe')
            this.$dispatch('add-prop', {'link':link})
        },
        showNotif: function() {
            var data = JSON.stringify(this.$root.id_pengaduan);

            this.$root.id_pengaduan = [];
            updateBadge(this.$root.id_pengaduan.length);

            this.frame('pengaduan/notif?id_pengaduan=' + data);
        }
    },
    events: {
        'refresh-dashboard': function() {
            this.getData();
        }
    },
    ready: function() {
        this.getData();

        setInterval(this.getData, 60000);

        var that = this;

        var socket = io.connect('http://128.199.109.135:8890');
        socket.on('pengaduan-baru', function (data) {
          that.getData();
        });
    }
});
